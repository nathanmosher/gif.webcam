'use strict';

angular.module('gifmehApp')
  .directive('projectPreview', function ($timeout, $rootScope, $q) {
    return {
      // template: '<div></div>',
      restrict: 'A',
      scope: {
        projectPreview: '='
      },
      link: function (scope, element, attrs) {
        // element.text('this is the projectPreview directive');
        var canvas = document.createElement('canvas');
            canvas.width = 300;
            canvas.height = 300;
        var ctx = canvas.getContext('2d');
        var fps = 30;
        var count = 0;
        var timeout = null;
        var imgs;

        element.append(canvas)


        var initialize = function () {

          // load up meh images

          var promises = _.map(scope.projectPreview.frames, function (frame) {
            var img = new Image;
            img.src = frame;
            var deferred = $q.defer();
            img.onload = function () {
              deferred.resolve(img)
            };
            return deferred.promise;
          });

          $q.all(promises).then(function (loadedImgs) {
            // console.log('images loaded: ', imgs)
            imgs = loadedImgs;

            startAnimation();
          });

        };

        var startAnimation = function () {
          var drawFrame = function () {
            timeout = setTimeout(function() {
              requestAnimationFrame(drawFrame);
              if (count > imgs.length-1) count = 0;
              ctx.drawImage(imgs[count], 0, 0, 300, 300);

              count++;
            }, 1000 / fps);
          };
          drawFrame();
        };

        var reinitialize = function () {
          stopAnimation();
          initialize();
        };

        var stopAnimation = function () {
          console.log('timeout: ', timeout);
          clearTimeout(timeout);
        };

        // element.on('mouseenter', function () {
        //   startAnimation();
        // });

        // element.on('mouseleave', function () {
        //   stopAnimation();
        // });

        // initialize();
        $rootScope.$on('project-service:update', function () {
          reinitialize();
        });

      }
    };
  });
