'use strict';

angular.module('gifmehApp')
  .directive('clipThumbnail', function ($interval) {
    return {
      templateUrl: 'app/clip/thumbnail/clip-thumbnail.html',
      restrict: 'A',
      scope: {
        clipThumbnail: '='
      },
      link: function (scope, element, attrs) {
        var canvas = document.createElement('canvas');
            canvas.width = 100;
            canvas.height = 100;
        var ctx = canvas.getContext('2d');
        var fps = 30;
        var count = 0;
        var timeout = null;
        var imgs;

        element.append(canvas)

        var initialize = function () {

          // load up meh images
          imgs = _.map(scope.clipThumbnail.frames, function (frame) {
            var img = new Image;
            img.src = frame;
            return img;
          });

          console.log('initialized with %s frames', imgs.length)

          ctx.drawImage(imgs[0], 0, 0, 100, 100);

        };

        var startAnimation = function () {
          var drawFrame = function () {
            timeout = setTimeout(function() {
              requestAnimationFrame(drawFrame);
              if (count > imgs.length-1) count = 0;
              ctx.drawImage(imgs[count], 0, 0, 100, 100);

              count++;
            }, 1000 / fps);
          };
          drawFrame();
        };

        var reinitialize = function () {
          stopAnimation();
          initialize();
        };

        var stopAnimation = function () {
          // console.log('timeout: ', timeout)
          clearTimeout(timeout);
        };

        element.on('mouseenter', function () {
          startAnimation();
        });

        element.on('mouseleave', function () {
          stopAnimation();
        });

        initialize();
      }
    };
  });
