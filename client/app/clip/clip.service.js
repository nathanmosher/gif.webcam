'use strict';

angular.module('gifmehApp')
  .service('clipService', function ($rootScope) {

    var self = this;

    /*

    {
      type: 'video' || 'image',
      source: 'webcam' || 'drag' || 'external',
      frames: [] (array of data uris)
    }

     */

    // AngularJS will instantiate a singleton by calling "new" on this function
    var _clips = [];

    self.newClip = function (clip) {
      _clips.push({
        interval: clip.interval,
        type: clip.type || 'unknown',
        source: clip.source || 'unkown',
        complete: false,
        frames: clip.frames || []
      });

      // Return id of this clip.
      return _clips.length-1;
    };

    self.getClip = function (id) {
      return _clips[id] || null;
    };

    self.setClip = function (id, newClip) {
      if(_clips[id]) {
        _clips[id] = _.extend(_clips[id], newClip);
        return _clips[id] || null;
      } else {
        console.log('could not set clip %s', id)
      }

    };

    self.addFrame = function (frame) {

      var id = frame.id;
      var dataUri = frame.data;

      if (_clips[id]) {
        _clips[id].frames.push(dataUri);
      } else {
        console.log('tried to add frame to clip %s but clip not found!', id);
      }

    }

    self.finishClip = function (id) {
      if (_clips[id]) {
        _clips[id].complete = true;
        $rootScope.$broadcast('clip-service:clip-completed', { clip: _clips[id], id: id });
      } else {
        console.log('tried to finish clip %s but clip not found!', id);
      }
    };

    return self;
  });
