'use strict';

angular.module('gifmehApp')
  .service('projectService', function (clipService, $rootScope, $q) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    //
    var self = this;

    self.fps = 30;
    self.seconds = 7;
    self.clips = [];
    self.height = 300;
    self.width = 300;
    self.frameCount = parseInt((self.seconds*1000)/self.fps);

    self.clips = []

    /*
      clip = {
        firstFrame: int
        lastFrame: int
        id: int
      }
     */

    self.appendClip = function (id, clip) {
      var lastClip = self.findLastClip();

      var newClip = {};

      if(lastClip) {
        newClip.firstFrame = lastClip.lastFrame + 1;
        newClip.lastFrame = newClip.firstFrame + clip.frames.length-1;
      } else {
        newClip.firstFrame = 0;
        newClip.lastFrame = clip.frames.length-1;
      }
      newClip.id = id;

      self.clips.push(newClip);
    };

    self.findLastClip = function () {
      var lastFrame = 0;
      var lastClip = null;
      _.each(self.clips, function (clip, i) {
        if (clip.lastFrame > 0) {
          lastFrame = clip.lastFrame;
          lastClip = clip;
        }
      });
      return lastClip;
    };

    self.findLastFrame = function () {
      var lastClip = self.findLastClip();
      return lastClip.lastFrame;
    };

    self.getAllFrames = function () {
      var projectFrames = [];
      for (var i = 0; i < self.findLastFrame(); i++) {
        projectFrames.push(self.getFrame(i));
      }
      return projectFrames;
    };

    self.getAllFramesAsImageTagsAsync = function () {
      var promises = _.map(self.getAllFrames(), function (frame) {
        var img = new Image;
        img.src = frame;
        var deferred = $q.defer();
        img.onload = function () {
          deferred.resolve(img)
        };
        return deferred.promise;
      });

      return $q.all(promises);
    };

    self.getFrame = function (projectFrameIndex) {
      var visibleClipMeta = null;

      _.each(self.clips, function (clip, i) {
        if (projectFrameIndex >= clip.firstFrame && projectFrameIndex <= clip.lastFrame) {
          visibleClipMeta = clip;
        }
      });

      var visibleClip = clipService.getClip(visibleClipMeta.id);



      var clipFrameIndex = projectFrameIndex - visibleClipMeta.firstFrame;

      var visibleFrame = visibleClip.frames[clipFrameIndex];


      if(visibleFrame == undefined) {
        console.log('visible frame:', visibleFrame);
        console.log('visible clip:', visibleClip);
        console.log('clip frame index:', clipFrameIndex);
      }

      return visibleFrame;
    };

    $rootScope.$on('clip-service:clip-completed', function (e, data) {
      self.appendClip(data.id, data.clip);
      $rootScope.$broadcast('project-service:update');
    });

  });
