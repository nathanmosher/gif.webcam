'use strict';

angular.module('gifmehApp')
  .directive('webcam', function (webcamService, clipService) {
    return {
      templateUrl: 'components/webcam/webcam.html',
      restrict: 'EA',
      controller: function ($scope, webcamService) {

        $scope.recording = false;
        $scope.word = 'hello';
        $scope.startRecording = function () {
          console.log('recording!')
          webcamService.startRecording();
          $scope.recording = true;
        };

        $scope.stopRecording = function () {
          webcamService.stopRecording();
          $scope.recording = false;
        };

      },
      // controllerAs: 'webby',
      require: ['^webcam'],
      link: function (scope, element, attrs, controller) {
        var webcamController = controller[0]
        var recordButton = angular.element(element[0].querySelector('.record'));
        var stopButton = angular.element(element[0].querySelector('.stop'));
        var videoEl = angular.element(element[0].querySelector('video'));
        var interval = null;

        videoEl.height = 300; // TODO
        videoEl.width = 300;

        webcamService.on('ready', function (stream) {
          videoEl[0].src = window.URL.createObjectURL(stream);
          // element.append(videoEl);
        });

        // recordButton.on('click', function (e) {
        //   webcamService.startRecording();
        //   webCamController.recording = true;
        // });

        // stopButton.on('click', function (e) {
        //   webcamService.stopRecording();
        //   webCamController.recording = false;
        // });


      }
    };
  });
