'use strict';

angular.module('gifmehApp')
  .service('webcamService', function ($interval, clipService) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    // Fallbacks
    navigator.getUserMedia = navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msGetUserMedia;

    var self = this;

    // Private properties.
    var _on = {};
    var _video = document.createElement('video');
        _video.setAttribute('autoplay', 'true');
        _video.height = 300; // TODO
        _video.width = 300;

    var _canvas = document.createElement('canvas');
        _canvas.width = 300;
        _canvas.height = 300;
    var _ctx = _canvas.getContext('2d');

    var _currentRecordingInterval = null;
    var _currentRecordingClipId = null;

    var _fps = 30;

    // Private Methods
    var _requestPermission = function () {
      console.log('requesting permission');
      navigator.getUserMedia({
          video: {
            mandatory: {
              minWidth: 300,
              minHeight: 300,
              maxWidth: 300,
              maxHeight: 300
            }
          }
        }, function (stream) {
            self.emit('ready', stream);
        }, function (error) {
            console.log(error)
            throw new Error(error);
        });
    };

    // Public properties.
    self.permitted = false;
    self.stream = null;
    self.recording = false;

    // Public methods.
    self.on = function (ev, callback) {
      _on[ev] = _on[ev] || [];
      _on[ev].push(callback);
    };

    self.emit = function (ev, data) {
      _on[ev].forEach(function (fn) {
        fn(data);
      });
    };

    self.snapshot = function (id) {
      if (self.stream) {
        _ctx.drawImage(_video, 0, 0, 300, 300);
        // "image/webp" works in Chrome.
        // Other browsers will fall back to image/png.
        if (id !== null) {
          var frame = {
            id: id,
            data: _canvas.toDataURL('image/webp')
          };
          clipService.addFrame(frame);
          // console.log(clipService.getClip(id));
        }
      }
    };

    self.startRecording = function () {

      self.recording = true;

      _currentRecordingClipId = clipService.newClip({
        type: 'video',
        interval: setInterval(function () {
          // console.log('about to snapshot')
          self.snapshot(_currentRecordingClipId)
        }, 1000/_fps),
        source: 'webcam',
        frames: []
      });

      console.log('newClip id: %s', _currentRecordingClipId)
      self.snapshot(_currentRecordingClipId);

    };

    self.stopRecording = function () {

      var currentRecordingClip = clipService.getClip(_currentRecordingClipId);

      console.log('recording interval', currentRecordingClip.interval);

      clearInterval(currentRecordingClip.interval);



      clipService.finishClip(_currentRecordingClipId);

      self.recording = false;
      _currentRecordingClipId = null;
      _currentRecordingInterval = null;

      // $rootScope.$apply()

    };


    // Events
    self.on('ready', function (stream) {
      self.stream = stream;
      self.permitted = true;
      _video.src = window.URL.createObjectURL(stream);
      console.log('now permitted!');
    });


    // Init
    _requestPermission();

    return self;
  });
