'use strict';

angular.module('gifmehApp')
  .controller('PreviewCtrl', function ($scope, $rootScope, projectService) {
    $scope.project = {};
    $scope.project.frames = [];
    $rootScope.$on('project-service:update', function (e) {
      console.log('get all the frames: ', projectService.getAllFrames())

      $scope.project.frames = projectService.getAllFrames();
    });

    $scope.gif = function () {
      var gif = new GIF({
        workers: 4,
        quality: 10,
        workerScript: 'assets/gif-encoder/gif.worker.js'
      });

      var promise = projectService.getAllFramesAsImageTagsAsync();

      promise.then(function (imagesEl) {
        _.each(imagesEl, function (imageEl, i) {
          gif.addFrame(imageEl, {delay: parseInt(1000/30)});
        });
        gif.render();
      });

      gif.on('finished', function(blob) {
          blob.slice = blob.mozSlice || blob.webkitSlice || blob.slice;

          var dlBlob = blob.slice(0, blob.size, 'octet/stream');

          var a = document.createElement("a");
          a.href = URL.createObjectURL(dlBlob);
          a.download = 'gifcam.gif';
          a.click();
      });


    };
  });
