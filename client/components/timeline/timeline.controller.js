'use strict';

angular.module('gifmehApp')
  .controller('TimelineCtrl', function ($scope, clipService, $rootScope) {

    $scope.clips = [];

    $rootScope.$on('clip-service:clip-completed', function (e, data) {
      console.log('event fired: ', e, data);
      $scope.clips.push(data.clip);
    });

  });
